#!/usr/bin/python

site_url = "http://localhost:8000"
ws_server_ip = "localhost"
ws_server_port = 8888
ws_server_external_port = 8888
log_enabled = True
timeout_connection = 1800
ping_interval = 100
use_ssl = False
