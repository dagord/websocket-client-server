////////////////////////////
// websocket scripting
////////////////////////////

var owner_id = "2";

var ws_status = false;
var ws_address="ws://localhost:8888/generic/browser_" + owner_id;

function ws_start(ws_address){

    isActive = true;

    socket = new WebSocket(ws_address);
    socket.onclose = function(event){
        //try to reconnect in 5 seconds
        if (event.code == 1006) {
            setTimeout(function(){ws_start(ws_start)}, 5000);
            websocket_closed();
        }
    };

    socket.onmessage = function(e) {
        console.log("--> * MESSAGE RECEIVED *");
        console.log(e.data);
    }

    socket.onopen = function() {
        ws_status = true;
    }

}

function websocket_closed() {
    ws_status = false;
}


function send_websocket_message(message_type, message_text, recipient_id, recipient_type) {

        var message_content = {
                "message": message_text,
                "type": message_type,
            };

        var message_sender = {
                "id": 1,
                "type": "browser",
            };

        var message_recipient = {
                "id": recipient_id,
                "type": recipient_type,
            };

        var message = {
                    "message_content": message_content,
                    "message_sender": message_sender,
                    "message_recipient": message_recipient,
            };

        message = JSON.stringify(message);

        console.log("* MESSAGE SENT * -->")
        console.log(message);

        if (ws_status) {
            socket.send(message)
        }
}

ws_start(ws_address);

function send() {
    send_websocket_message("message", $("#send_text").val(), $("#rpi_id").val(), "rpi");
    $("#send_text").val("");
}
