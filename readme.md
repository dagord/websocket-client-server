** Sample WEBSOCKET SERVER / CLIENT **

Install requirements:

`pip install requests`

`pip install autobahn`

`pip install twisted`


The script is very simple and options are self explanatory.

Launch python websocket server:
`python websocket_server.py`

Launch python websocket client:
`python websocket_client.py $RPI_ID $OWNER_ID`

(where: $RPI_ID is the client id and $OWNER_ID is the owner the client is associated to; useful if you have to sort message between different users)

for example:
`python websocket_client.py 12345 2`

The client will connect to the server and exchange some welcome messages.

** Browser client **

If you want to send/receive messages to/from your client, just point your browser to the **index.html** file.
Please edit **js/websocket_client.js**, replacing the **owner_id** parameter to match it to the websocket_client **$OWNER_ID**

* Type some text into the "message" box
* Type the RPI_ID into the "client id" box (default: 12345)
* Click on "Send" and look at your websocket_client.py output!

To write a message back to your browser, open your CLI and type:
`echo "some text" > incoming`

The client will listen for text from the "incoming" FIFO and will echo them back to the server; look for console messages from your index.html page!

The scripts can be easily extended with authentication methods, multi-user/multi-client interface and so on.

Enjoy! ;)