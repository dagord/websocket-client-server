#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, json
from urlparse import urlparse
import json, time, datetime
from threading import Thread

from autobahn.twisted.websocket import WebSocketClientProtocol, WebSocketClientFactory
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.internet import reactor, task
from twisted.python import log

import config_common

try:
    rpi_id = sys.argv[1]
    owner_id = sys.argv[2]
    client_type = "rpi"
except:
    message = "You need to specify your $RPI_ID and $OWNER_ID!\nEs: python websocket_client.py 12345 2"
    quit(message)

class MyClientProtocol(WebSocketClientProtocol):

    def onOpen(self):

        self.send_message("message", "Connected", "browser")
        time.sleep(1)

        # launch a backgorund thread to listen to external inputs
        try:
            th._stop()
        except:
            pass
        th = Thread(target=self.listen_external_messages)
        th.start()

    def onMessage(self, message, isBinary):
        if isBinary:
            print("Binary message received: {0} bytes".format(len(payload)))
        else:
            message = json.loads(message)
            message_content = message['message_content']
            message_sender = message['message_sender']
            message_recipient = message['message_recipient']

            print "Message received: %s" % message_content['message']

            if message_content['message'] == "get_status":
                self.send_message('message', 'connected', 'browser')

    def send_message(self, msg_type, msg_content, recipient_type):

        print "Message sent: %s" % msg_content

        message = {}
        message_content = {}
        message_content['type'] = msg_type
        message_content['message'] = msg_content

        message_sender = {}
        message_sender['id'] = rpi_id
        message_sender['type'] = client_type

        message_recipient = {}
        message_recipient['id'] = 0
        message_recipient['type'] = recipient_type

        message['message_content'] = message_content
        message['message_sender'] = message_sender
        message['message_recipient'] = message_recipient

        self.sendMessage(json.dumps(message), isBinary=False)

    def connection_lost(self, exc):
        print "Disconnected"

    def listen_external_messages(self):
        print "Waiting for external inputs..."
        FIFO = 'incoming'

        # tries to delete FIFO and queued messages (if any)
        # then creates it again
        try:
            os.unlink(FIFO)
        except:
            pass

        try:
            os.mkfifo(FIFO)
        except:
            pass

        # resta in attesa degli input
        with open(FIFO) as fifo:
            while True:
                line = fifo.readline()
                if line != '':
                    self.send_message('message', line, 'browser')
                time.sleep(0.1)


class MyClientFactory(WebSocketClientFactory, ReconnectingClientFactory):

    protocol = MyClientProtocol
    maxDelay = 10

    def clientConnectionFailed(self, connector, reason):
        print("Client connection failed .. retrying ..")
        self.retry(connector)

    def clientConnectionLost(self, connector, reason):
        print("Client connection lost .. retrying ..")
        self.retry(connector)


if __name__ == '__main__':

    url = "ws://%s:%s" % (config_common.ws_server_ip, config_common.ws_server_external_port)

    print url

    from twisted.internet import reactor
    log.startLogging(sys.stdout)

    headers = {
        'client_id': rpi_id,
        'client_type': 'rpi',
        'owner_id': owner_id,
        'channel': 'generic'
        }

    factory = MyClientFactory(url, headers= headers)
    reactor.connectTCP(config_common.ws_server_ip, config_common.ws_server_external_port, factory)
    reactor.run()
