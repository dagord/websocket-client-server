#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
This is a simple Websocket Echo server that uses the Twisted/Autobahn server.

pip install twisted
pip install autobahn
(and if required)
pip install service-identity
'''

import os, sys, json, time, datetime, requests
from urlparse import urlparse

from autobahn.twisted.websocket import WebSocketServerProtocol, WebSocketServerFactory
from twisted.internet import reactor, task

import config_common


connected_devices = {}

class WSHandler(WebSocketServerProtocol):

    connections = []

    def onConnect(self, request):

        '''this is the handshake function; it is triggered when a client tries to connect'''

        '''
        Browser connection string:
        ws://localhost:8888/general/browser_1
        Remote device connection string:
        ws://localhost:8888
        '''

        global config_common

        # if the client is a "browser", its identity must be get from connection string
        try:
            client_temp = request.path.split('/')[-1]
            self.client_type = client_temp.split('_')[0]
            self.client_id = client_temp.split('_')[1]

        # if the client is a "rpi" (remote device), its identity must be get from headers
        except:
            print request.headers
            if 'client_id' in request.headers:
                self.id = request.headers['client_id']
                self.client_id = request.headers['client_id']

            if 'client_type' in request.headers:
                self.client_type = request.headers['client_type']

            if 'owner_id' in request.headers:
                self.owner_id = request.headers['owner_id']


        authorization = True

        if self.client_type == "browser":
            self.user_type = "User"
            self.owner_id = self.client_id

        elif self.client_type == "rpi":
            self.user_type = "Rpi"

        else:
            authorization = False

        # if the client is not authorized, connection is closed
        if not authorization:
            self.write_log("Rejected")
            self.sendClose()
            time.sleep(1)

        else:
            self.connections.append(self)
            self.write_log("Accepted")

    def onOpen(self):
        '''this function is triggered when handshake has been successfully completed'''

        message_content = {}
        message_content['message'] = "Accepted"
        message_content['type'] = "message"

        self.send_message(message_content, self.client_id, self.client_type)


    def send_message(self, message_content, recipient_id, recipient_type):
        '''this function composes the message to be sent'''

        print "Message to be sent (to %s): %s" % (recipient_id, message_content)

        message = {}

        message_sender = {}
        message_sender['id'] = self.client_id
        message_sender['type'] = self.client_type

        message_recipient = {}
        message_recipient['id'] = recipient_id
        message_recipient['type'] = recipient_type

        message['message_content'] = message_content
        message['message_sender'] = message_sender
        message['message_recipient'] = message_recipient

        for conn in self.connections:
            if (
            (recipient_type == "rpi" and conn.client_type == recipient_type
            and str(conn.client_id) == str(recipient_id))
            or
            (recipient_type == "browser" and conn.client_type == recipient_type
            and (self.owner_id == conn.owner_id or conn.user_type == "Admin"))
            ):
                conn.sendMessage(json.dumps(message), isBinary=False)

    def onMessage(self, content, isBinary):
        '''this function is triggered when ws server receives an incoming message'''
        ammesso = True

        try:
            message = json.loads(content.decode('utf8'))
            message_content = message['message_content']
        except:
            message = {}
            ammesso = False

        if ammesso:
            self.send_message(message_content, message['message_recipient']['id'], message['message_recipient']['type'])

        else:
            ammesso = False

        if not ammesso:
            print "message not allowed:"
            print message

    def onClose(self, wasClean, code, reason):
        '''this function is triggered when a client disconnects'''
        try:
            print "*** %s %s Disconnected ***" % (self.client_id, self.client_type)
            if self.client_type == "rpi":
                message_content = {}
                message_content['type'] = "message"
                message_content['message'] = "Disconnected"
                self.send_message(message_content, self.owner_id, "browser")
            self.write_log(str(self.id) + ": disconnesso 0 " + self.client_type + " (" + self.client_id + ")")
        except:
            pass

        try:
            self.connections.remove(self)
        except:
            pass

    def onPong(self, data):
        if self.client_type == "rpi":
            connected_devices[self.client_id] = time.time()

    def check_origin(self, origin):
        return True

    def write_log(self, message):
        if config_common.log_enabled:
            print time.strftime('%d/%m/%y - %H:%M:%S | ' + str(message))
            message_to_log = time.strftime('%d/%m/%y - %H:%M:%S | ') + str(message) + '\n'
            try:
                file_log = open(current_directory + '/websocket.log', 'a')
                file_log.write(message_to_log)
                file_log.close()
            except:
                pass


if __name__ == "__main__":

    print "Launcing websocket server"
    print "ws://localhost:%s" % config_common.ws_server_port

    factory = WebSocketServerFactory()
    factory.protocol = WSHandler
    factory.setProtocolOptions(autoPingInterval=config_common.ping_interval,
        autoPingTimeout=int(config_common.ping_interval/2))
    if config_common.use_ssl:
        contextFactory = ssl.DefaultOpenSSLContextFactory(
            settings.SSL_PRIVATEKEY, settings.SSL_CERTFILE)
        reactor.listenSSL(config_common.ws_server_port, factory, contextFactory)
    else:
        reactor.listenTCP(config_common.ws_server_port, factory)

    reactor.run()
